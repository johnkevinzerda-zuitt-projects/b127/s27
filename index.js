let http = require('http');

let mockDirectory =[
		{
			"firstName": "Mary Jane",
			"lastName": "Dela Cruz",
			"mobileNo": "09123456789",
			"email": "mjdelacruz@mail.com",
			"password": 123
		},
		{
			"firstName": "John",
			"lastName": "Doe",
			"mobileNo": "09123456789",
			"email": "jdoe@mail.com",
			"password": 123
		}
	]

http.createServer(function(req,res){
	if (req.url == "/profile" && req.method == "GET") {
		res.writeHead(200,{'Content-type':'application/json'});
		res.write(JSON.stringify(mockDirectory));
		res.end()
	}
	if (req.url == "/profile" && req.method == "POST") {

		let requestBody ='';
		req.on('data',function(data){
			requestBody += data;
		})
		req.on('end',function(){
			requestBody = JSON.parse(requestBody)

			let newUser = {
				"firstName": requestBody.firstName,
				"lastName": requestBody.lastName,
				"mobileNo": requestBody.mobileNo,
				"email": requestBody.email,
				"password": requestBody.password
			}

			mockDirectory.push(newUser);
			res.writeHead(200,{'Content-type':'application/json'});
			res.write(JSON.stringify(mockDirectory));
			res.end()
		})

	}
}).listen(4000);

console.log('Server running @localHost: 4000');